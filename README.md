# OpenML dataset: white-clover

https://www.openml.org/d/343

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Ian Tarbotton  
**Source**: [original](http://www.cs.waikato.ac.nz/ml/weka/datasets.html) -   
**Please cite**:   

White Clover Persistence Trials

Data source:   Ian Tarbotton
AgResearch, Whatawhata Research Centre, Hamilton, New Zealand

The objective was to determine the mechanisms which influence the persistence of white clover populations in summer dry hill land. In particular reference to the consequence of a severe summer dry period in 1993/1994 and how it impacted on the performance of three white clover cultivars in an on-going experiment located at Whatawhata Research Centre.

The machine learning objective was to predict the amount of white clover in 1994 from the amount of white clover and other species in the years 1991 to 1994 as well as information on the 'strata' where the white clover was being grown.

Attribute Information:
1.  strata - enumerated
2.  plot - enumerated
3.  paddock - enumerated
4.  WhiteClover-91 - white clover measurement in 1991 - real
5.  BareGround-91 - bare ground measurement in 1991 - real
6.  Cocksfoot-91 - cocksfoot measurement in 1991 - real
7.  OtherGrasses-91 - other grasses measurement in 1991 - real
8.  OtherLegumes-91 - other legumes measurement in 1991 - real
9.  RyeGrass-91 - ryegrass measurement in 1991 - real
10. Weeds-91 - weeds measurement in 1991 - real
11. WhiteClover-92 - white clover measurement in 1992 - real
12. BareGround-92 - bare ground measurement in 1992 - real
13. Cocksfoot-92 - cocksfoot measurement in 1992 - real
14. OtherGrasses-92 - other grasses measurement in 1992 - real
15. OtherLegumes-92 - other legumes measurement in 1992 - real
16. RyeGrass-92 - ryegrass measurement in 1992 - real
17. Weeds-92 - weeds measurement in 1992 - real
18. WhiteClover-93 - white clover measurement in 1993 - real
19. BareGround-93 - bare ground measurement in 1993 - real
20. Cocksfoot-93 - cocksfoot measurement in 1993 - real
21. OtherGrasses-93 - other grasses measurement in 1993 - real
22. OtherLegumes-93 - other legumes measurement in 1993 - real
23. RyeGrass-93 - ryegrass measurement in 1993 - real
24. Weeds-93 - weeds measurement in 1993 - real
25. BareGround-94 - bare ground measurement in 1994 - real
26. Cocksfoot-94 - cocksfoot measurement in 1994 - real
27. OtherGrasses-94 - other grasses measurement in 1994 - real
28. OtherLegumes-94 - other legumes measurement in 1994 - real
29. RyeGrass-94 - ryegrass measurement in 1994 - real
30. Weeds-94 - weeds measurement in 1994 - real
31. strata-combined - enumerated
32.  WhiteClover-94 - white clover measurement in 1994 - enumerated

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/343) of an [OpenML dataset](https://www.openml.org/d/343). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/343/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/343/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/343/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

